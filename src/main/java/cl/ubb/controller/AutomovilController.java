package cl.ubb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import cl.ubb.model.*;
import cl.ubb.service.*;
import static org.springframework.http.HttpStatus.*;

@RequestMapping("/renTa ")
@RestController
public class AutomovilController {

	@Autowired
	private AutomovilService autoService;
	
	@RequestMapping (value = "/obtenerAutomovilesPorCategoria", method = GET)
	@ResponseBody
	public ResponseEntity<List<Automovil>> ObtenerAutomovilesPorCategoria(@PathVariable("categoria") String categoriaAuto){
		ResponseEntity<List<Automovil>> response;
		
		List<Automovil> list=autoService.obtenerAutomovilesPorCategoria(categoriaAuto);
		response = new ResponseEntity<List<Automovil>>(list, OK);
		return response;
	}
}
