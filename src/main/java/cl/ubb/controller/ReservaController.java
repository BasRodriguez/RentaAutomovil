package cl.ubb.controller;

import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import cl.ubb.model.*;
import cl.ubb.service.*;
import static org.springframework.http.HttpStatus.*;


@RequestMapping("/renTa ")
@RestController
public class ReservaController {

	@Autowired
	private ReservaService reservaService;
	
	@RequestMapping (value = "/reservarAuto", method = POST)
	@ResponseBody
	public ResponseEntity<Reserva>ReservarAuto(@RequestBody Reserva reserva){
		Cliente cliente = new Cliente(); 
		reservaService.reservarAuto(cliente.getId(), Matchers.anyString(), Matchers.anyString(), Matchers.anyString());
		return new ResponseEntity<Reserva>(reserva, HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/listarReservas", method = GET)
	@ResponseBody
	public ResponseEntity<List<Reserva>> ListarReservas(){
		ResponseEntity<List<Reserva>> response;
		Cliente cliente = new Cliente(); 
		Reserva reserva = new Reserva();
		
		List<Reserva> list=reservaService.listarReservasDeCliente(cliente.getId(), reserva.getFechaInicio());
		response = new ResponseEntity<List<Reserva>>(list, OK);
		return response;
	}
}
