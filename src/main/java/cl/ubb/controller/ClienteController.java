package cl.ubb.controller;

import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import cl.ubb.model.*;
import cl.ubb.service.*;
import static org.springframework.http.HttpStatus.*;


@RequestMapping("/renTa ")
@RestController
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	

	@RequestMapping (value = "/registrarCliente", method = POST)
	@ResponseBody
	public ResponseEntity<Cliente>RegistrarCliente(@RequestBody Cliente cliente){
		clienteService.registroCliente(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/obtenerTodos", method = GET)
	@ResponseBody
	public ResponseEntity<List<Cliente>> ObtenerTodos(){
		ResponseEntity<List<Cliente>> response;
		
		List<Cliente> list=clienteService.obtenerTodos();
		response = new ResponseEntity<List<Cliente>>(list, OK);
		return response;
	}
	
	@RequestMapping (value = "/obtenerPorRut/{rut}", method = GET)
	@ResponseBody
	public ResponseEntity<Cliente>ObtenerPorRut(@RequestBody Cliente cliente){
		clienteService.obtenerPorRut(Matchers.anyString());
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}
	
	
	
	
}
