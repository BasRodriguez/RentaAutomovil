package cl.ubb.controller;

import static org.junit.Assert.*;

import static org.mockito.Matchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.model.*;
import cl.ubb.service.*;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.apache.http.HttpStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class RentaControllerTest {
	
	@Mock
	private ClienteService clienteService;
	private ReservaService reservaService;
	private AutomovilService autoService;
	
	@InjectMocks
	private ClienteController clienteController;
	
	
	@Test
	public void registrarCliente(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setRut("17.748.310-9");
		cliente.setNombre("Bass");
		cliente.setCelular(98520939);
		cliente.setEmail("bass@gmail.com");
		
		Mockito.when(clienteService.registroCliente(Matchers.any(Cliente.class))).thenReturn(cliente);
		
		given().
			contentType(ContentType.JSON).
			body(cliente).
		when().
			post("/renTa/registrarCliente").
		then().
			statusCode(SC_CREATED).
			assertThat().
			body("nombre", equalTo("Bass"));
			
	}
	
	@Test 
	public void listarClientes(){
		List<Cliente> clientes=new ArrayList<Cliente>();
		
		Mockito.when(clienteService.obtenerTodos()).thenReturn((ArrayList<Cliente>) clientes);
		
		given().contentType(ContentType.JSON).
		when().get("/renTa/obtenerTodos").
		then().body("rut", equalTo("17.748.310-9") ).
		statusCode(200);
	}
	
	@Test
	public void listarAutosPorCategoria(){
		List<Automovil> autos = new ArrayList<Automovil>();
		
		Mockito.when(autoService.obtenerAutomovilesPorCategoria(Matchers.anyString())).thenReturn((ArrayList<Automovil>) autos);
		
	}
	
	@Test
	public void estaONoRegistrado(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		
		cliente.setId(1L);
		cliente.setRut("17.748.310-9");
		cliente.setNombre("Bass");
		cliente.setCelular(98520939);
		cliente.setEmail("bass@gmail.com");
		
		Mockito.when(clienteService.obtenerPorRut(Matchers.anyString())).thenReturn(cliente.getNombre());
		
		given().
		when().
			get("/renTa/obtenerPorRut/{rut}","17.748.310-9").
		then().
			statusCode(SC_OK).
			assertThat().
			body("rut", equalTo("17.748.310-9"));		
	}
	
	@Test
	public void reservarAuto(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		Automovil auto = new Automovil();
		Reserva reserva = new Reserva();
		List <Reserva> reservas = new ArrayList<Reserva>();
		
		cliente.setId(1L);
		cliente.setRut("17.748.310-9");
		cliente.setNombre("Bass");
		cliente.setCelular(98520939);
		cliente.setEmail("bass@gmail.com");
		
		auto.setCategoria("Deportivo");
		auto.setId(1L);
		auto.setMarca("Ferrari");
		auto.setModelo("California T");
		auto.setTipoTransmicion("Automatico");
		
		reserva.setId(1L);
		reserva.setIdAutomovil(auto.getId());
		reserva.setIdCliente(cliente.getId());
		reserva.setFechaInicio("16/11/2017");
		reserva.setFechaFin("01/12/2017");
		
		reservas.add(reserva);
		
		Mockito.when(reservaService.reservarAuto(cliente.getId(), reserva.getFechaInicio(), reserva.getFechaFin(), auto.getCategoria())).thenReturn((ArrayList<Reserva>) reservas);
		
		given().
			contentType(ContentType.JSON).
		when().
			post("/renTa/reservarAuto").
		then().
			statusCode(SC_CREATED).
			assertThat();
		
	}
	
	@Test
	public void listarReservas(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		Automovil auto = new Automovil();
		Reserva reserva = new Reserva();
		ArrayList<Reserva> misReservas = new ArrayList<Reserva>();
		
		cliente.setId(1L);
		cliente.setRut("17.748.310-9");
		cliente.setNombre("Bass");
		cliente.setCelular(98520939);
		cliente.setEmail("bass@gmail.com");
		
		auto.setCategoria("Deportivo");
		auto.setId(1L);
		auto.setMarca("Ferrari");
		auto.setModelo("California T");
		auto.setTipoTransmicion("Automatico");
		
		reserva.setId(1L);
		reserva.setIdAutomovil(auto.getId());
		reserva.setIdCliente(cliente.getId());
		reserva.setFechaInicio("16/11/2017");
		reserva.setFechaFin("01/12/2017");
		Mockito.when(reservaService.listarReservasDeCliente(cliente.getId(), reserva.getFechaInicio())).thenReturn((ArrayList<Reserva>) misReservas);
		
		given().contentType(ContentType.JSON).
		when().get("/renTa/listarReservas").
		then().body("rut", equalTo("17.748.310-9") ).
		statusCode(200);
		
		
	}

}
