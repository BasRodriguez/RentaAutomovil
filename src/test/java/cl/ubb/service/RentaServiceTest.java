package cl.ubb.service;

import static org.junit.Assert.*;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyLong;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.dao.ClienteDao;
import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Automovil;
import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;

import org.junit.Before;
import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class RentaServiceTest {
		
	@Mock
	private AutomovilDao automovilDao;
	private ClienteDao clienteDao;
	private ReservaDao reservaDao;
		
	@InjectMocks
	private AutomovilService automovilService;
	private ClienteService clienteService;
	private ReservaService reservaService;
	
	private Cliente cliente;
	private Cliente clienteRegistrado;
	private Automovil automovil;
	private Reserva reserva;
	private ArrayList<Reserva> reservaRealizada;
	
	@Before
	public void SetUp(){
		cliente = new Cliente();
		automovil = new Automovil();
		reserva = new Reserva();
	}
	
	@Test
	public void RegistrarCliente(){
		//arrange
		
		//act
		doNothing().when(clienteDao).save(cliente);
		clienteRegistrado = clienteService.registroCliente(cliente);
		
		//assert
		Assert.assertNotNull(clienteRegistrado);
		Assert.assertEquals(cliente, clienteRegistrado);
		verify(clienteDao).save(cliente);
	}
	
	@Test 
	public void ListarTodosLosClientes() throws Exception{
		//arrange
		ArrayList<Cliente> misClientes = new ArrayList<Cliente>();
		ArrayList<Cliente> resultadoClientes = new ArrayList<Cliente>();
		
		//act
		when(clienteDao.findAll()).thenReturn(misClientes);
		resultadoClientes = clienteService.obtenerTodos();
				
		//assert
		Assert.assertNotNull(resultadoClientes);
		Assert.assertEquals(resultadoClientes, misClientes);
	}
	
	@Test
	public void obtenerAutomovilesPorCategoria(){
		//arrange
		ArrayList<Automovil> misAutomoviles = new ArrayList<Automovil>();
		ArrayList<Automovil> resultadoAutomoviles = new ArrayList<Automovil>();
		automovil.setCategoria("Lujo, Transporte, CityCar");
		
		//act
		when(automovilDao.findAll()).thenReturn(misAutomoviles);
		resultadoAutomoviles = automovilService.obtenerAutomovilesPorCategoria("Lujo, Transporte, CityCar");
		
		//assert
		Assert.assertNotNull(resultadoAutomoviles);
		Assert.assertEquals(resultadoAutomoviles, misAutomoviles);
	}
	
	@Test
	public void IndicarSiClienteEstaRegistrado(){
		//arrange
		String clienteRegistro;
		
		//act
		when(clienteDao.findByRut(anyString())).thenReturn(cliente);
		clienteRegistro = clienteService.obtenerPorRut(cliente.getRut());
		
		//assert
		Assert.assertNotNull(clienteRegistro);
		Assert.assertEquals("si", clienteRegistro);
	}
	
	@Test
	public void IndicarSiClienteNoEstaRegistrado(){
		//arrange
		String clienteRegistro;
		
		//act
		when(clienteDao.findByRut(anyString())).thenReturn(cliente);
		clienteRegistro = clienteService.obtenerPorRut(cliente.getRut());
		
		//assert
		Assert.assertNotNull(clienteRegistro);
		Assert.assertEquals("no", clienteRegistro);
	}
	
	@Test
	public void ReservarAuto(){
		//arrange
		long id = cliente.getId();
		String fechainicio =  reserva.getFechaInicio();
		String fechafin = reserva.getFechaFin();
		String categoria = automovil.getCategoria();
		//act
		doNothing().when(reservaDao).save(reserva);
		reservaRealizada = reservaService.reservarAuto(id, fechainicio, fechafin, categoria);
		
		//assert
		Assert.assertNotNull(reservaRealizada);
		Assert.assertEquals(reserva, reservaRealizada);
		verify(reservaDao).save(reserva);
	}
	
	@Test
	public void ListarReservasDeClienteDesdeUnaFecha(){
		//arrange
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		ArrayList<Reserva> misReservas = new ArrayList<Reserva>();
		long id = cliente.getId();
		String fechainicio =  reserva.getFechaInicio();
		
		//act
		when(reservaDao.findAll()).thenReturn(reservas);
		misReservas = reservaService.listarReservasDeCliente(id, fechainicio);
		
		//assert
		Assert.assertNotNull(misReservas);
		Assert.assertEquals(reservas, misReservas);
	}
}